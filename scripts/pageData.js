const data = {
    introduction: {
        class: "introduction",
        header: "The Journey So Far...",
        content: [
            "Following a successful launch in 2012, Condor Gaming Group has become a force to be reckoned with within the iGaming industry. In recent years, the company has soared to staggering heights. To date, Condor Gaming Group owns and operates 9 innovative online casinos and sportsbook brands, has offices spread across Europe and employs over 100 diverse employees."
        ]
    },
    aboutUs: {
        class: "about-us",
        header: "Who We Are",
        content: [
            "We are a hard-working and ambitious company with plans to revolutionize the iGaming industry. Our main focus is our users and the development of innovative gaming experiences.",
            "At Condor Gaming Group, we believe there are strategic keys to success. We offer top-tier games with intuitive interfaces from industry-leading game providers, peace of mind regarding player security and confidentiality, and the promise of fast withdrawals plus easy deposits through a variety of payment methods.",
            "We believe in individuality and play without limits. We strive to bring the best diverse, tailor-made gaming experiences to a wide array of players around the world through an ever increasing number of ways to have fun online."
        ]
    },
    portfolio: {
        class: "portfolio",
        header: "Our Portfolio",
        content: [
            "Our company’s values and philosophy is reflected in each of our six brands. With a unique set of characteristics each brand aims to provide the ultimate user-centric gaming experience."
        ],
    },
    contactUs: {
        class: "contact-us",
        header: "Contact",
        content: [
            "Get In Touch! Our team is here to help you with any questions you may have."
        ]
    },


    brands: [
        {
            src: "https://condorgaminggroup.com/wp-content/uploads/2022/10/brands-tournaverse-logo@2x.png",
            name: "Tournaverse",
            content: "A rocket-propelled flight full of an element of surprise is what you will experience in Tournaverse! Get on board and be prepared for this latest brand will deliver exhilarating gamification at its best. Journey with AstroBoy as you Level-Up from one planet to the next to reap additional Loyalty Points on top of the zooming daily rewards!",
            website: "https://www.tournaverse.com/en",
        },
        {
            src: "https://condorgaminggroup.com/wp-content/uploads/2022/10/brands-gambeta-logo@2x.png",
            name: "Gambeta10",
            content: "Dribble your way to fun and excitement with Gambeta10! When you’re in the penalty area surrounded by challenges with no one to pass the ball, nerves of steel from your end are what’s going to rile your fans to yell GOAL in this adrenaline-packed event. In this screamer of a match, seize the opportunity with Gambeta10, so bet on your favorite sporting events and teams, but first put on the “10” and Make Your Best Move!",
            website: "https://www.gambeta10.com/en/"
        },
        {
            src: "https://condorgaminggroup.com/wp-content/uploads/2022/10/brands-pedalada-logo@2x.png",
            name: "Pedalada10",
            content: "Give it all, for if you love the game, then you have come to the right online brand, Pedalada10! Score a ton of golos rewards with Pedalada10’s bevy of the casino, live casino, and sportsbook’s end-to-end choice of derby games. Premier graphics supported by a super-studded line-up of software providers, coupled with massive bonuses, and extra rewards welcome all who join and partake in this fast paced online world of Pedalada10, where Ousadia & Fantasia reign!",
            website: "https://www.pedalada10.com/en"
        },
        {
            src: "	https://condorgaminggroup.com/wp-content/uploads/2022/05/brands-24bettle-logo@2x.png",
            name: "24Bettle",
            content: "Teeming with gamified greatness, 24Bettle gives players full control to choose between the style, level of play, and rewards that are on offer. Enjoy the thrill of the game journeying with Carlos the Bull by selecting one of the 4 modes of play that suits you best and over 100 levels to slay! At 24Bettle, players are in for a neverending supply of entertainment.",
            website: "https://www.24bettle.com/en/"
        },
        {
            src: "https://condorgaminggroup.com/wp-content/uploads/2022/05/brands-b-bets-logo@2x.png",
            name: "b-Bets",
            content: "One for the die-hard sports fans, b-Bets is home to an endless supply of casino and sportsbook action, offering a wide range of casino and live casino games, plus live and virtual sports betting. The unique auctions feature allows players to be rewarded for their loyalty, as they accumulate BidBets.",
            website: "https://www.b-bets.com/en/"
        },
        {
            src: "https://condorgaminggroup.com/wp-content/uploads/2022/05/brands-big5casino-logo@2x.png",
            name: "Big5 Casino",
            content: "Set amid the African Savannah, Big5Casino offers a beastie experience led by Africa’s five iconic animals, the Buffalo, Leopard, Rhino, Elephant, and The Lion King. This is the 2nd brand that has adopted a multitude of gamification elements, with a level-up system that rewards players with Evolution points for their loyalty.",
            website: "https://www.big5casino.com/en/"
        },
        {
            src: "https://condorgaminggroup.com/wp-content/uploads/2022/05/brands-sieger-casino-logo@2x.png",
            name: "Sieger Casino",
            content: "Casino Sieger offers a no-nonsense casino experience like no other for born winners. It’s a popular pick for players in the DACH region. There is a multitude of features, including the Build-Your-Bonus feature, offering astounding rewards for loyal players.",
            website: "https://www.casinosieger.com/en/"
        },
        {
            src: "https://condorgaminggroup.com/wp-content/uploads/2022/05/brands-rembrandt-casino-logo@2x.png",
            name: "Rembrandt Casino",
            content: "A casino experience consisting of complete luxury where VIP treatment is lavished on all valued members. Rembrandt Casino features a wide selection of premium quality casino games and a world-class sportsbook, together with a Loyalty Boutique that is full of opulent rewards.",
            website: "https://www.rembrandtcasino.com/en/"
        },
        {
            src: "https://condorgaminggroup.com/wp-content/uploads/2022/05/brands-lucky-bull-logo@2x.png",
            name: "Lucky Bull",
            content: "LuckyBull offers a fresh and exciting burst of entertainment as the 3rd brand is overloaded with gamification. This casino and its comic book theme, endless adventures throughout time and space, and an achievement system packed with interactive features make for an unreal adventure, where the fun never comes to an end.",
            website: "https://www.luckybull.com/en/"
        }
    ]
}

export default data;