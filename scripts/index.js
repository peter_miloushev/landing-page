import pageData from './pageData.js';

const loadHeaderAndContent = (data) => {
    $(`.${data.class}-header`).text(data.header);
    $(`.${data.class}`).prepend(data.content.map(p => `<p>${p}</p>`));
}

const loadBrand = (data) => {
    return `<div class="card">
                <img class="card-img-top" src="${data.src}" alt="${data.name} logo">
                <div class="card-body">
                    <h5 class="card-title">${data.name}</h5>
                    <p class="card-text">${data.content}</p>
                    <a href="${data.website}" class="btn btn-primary visit-brand-button">Visit Brand Website</a>
                </div>
            </div>`
}

loadHeaderAndContent(pageData.introduction);
loadHeaderAndContent(pageData.aboutUs);
loadHeaderAndContent(pageData.portfolio);
loadHeaderAndContent(pageData.contactUs);

pageData.brands.forEach(brand => $(".brands-grid").append(loadBrand(brand)));

$("#company-information").accordion({
    heightStyle: "content",
    animate: {
        duration: 1500,
        easing: "easeOutExpo"
    },
    collapsible: true,
    active: false
});

$(".contact-form").submit(function (e) {
    e.preventDefault();

    const formData = {
        name: $("#inputName").val(),
        email: $("#inputEmail").val(),
        subject: $("#inputSubject").val(),
        message: $("#inputMessage").val()
    };

    this.reset();
});